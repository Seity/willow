﻿/**
 * Copyright (c) 2016 MultiFred
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using Willow.Model;
using System.IO;

namespace Willow.Unity
{
    public abstract class GraphEditor<TScriptableGraph, TSerializer, TGraph, TNode, TNodeValue, TEdge, TEdgeValue> : EditorWindow
        where TNode : Node<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TEdge : Edge<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TGraph : Graph<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TSerializer : GraphSerializer<TGraph, TNode, TNodeValue, TEdge, TEdgeValue>
        where TScriptableGraph : ScriptableGraphs<TSerializer, TGraph, TNode, TNodeValue, TEdge, TEdgeValue>
    {
        protected enum EdgeDirection { HORIZONTAL, INVERTED_HORIZONTAL, VERTICAL, INVERTED_VERTICAL}

        /// <summary>
        /// Getter setter of the ScriptableGraph displayed by the editor
        /// </summary>
        public TScriptableGraph CurrentScriptableGraph
        {
            get
            {
                return _currentScriptableGraph;
            }
            set
            {
                _currentScriptableGraph = value;
                if (_currentScriptableGraph != null)
                    _currentSerializedObject = new SerializedObject(_currentScriptableGraph);
                else
                    _currentSerializedObject = null;
            }
        }
        /// <summary>
        /// private field of the ScriptableGraph displayed by the editor
        /// </summary>
        private TScriptableGraph _currentScriptableGraph;

        /// <summary>
        /// SerializedObject associated to currentScriptableGraph
        /// </summary>
        protected SerializedObject _currentSerializedObject;

        /// <summary>
        /// the current graph as a TSerializer
        /// </summary>
        protected TSerializer target;

        /// <summary>
        /// The SerializedProperty associated to target
        /// </summary>
        public SerializedProperty targetProperty { get; protected set; }

        /// <summary>
        /// node count of target
        /// </summary>
        private int _nodeCount;

        /// <summary>
        /// Edge count of target
        /// </summary>
        private int _edgeCount;

        /// <summary>
        /// the index of the selected graph
        /// </summary>
        private int _currentSelection = 0;

        /// <summary>
        /// Scroll view position of the menu
        /// </summary>
        private Vector2 _menusScrollViewPosition;

        /// <summary>
        /// Rect of the menu window
        /// </summary>
        private Rect _menuWindowRect = new Rect(0, 0, 250, 800);

        /// <summary>
        /// should the Edge windows be displayed
        /// </summary>
        private bool _shouldDisplayEdgeWindows = true;

        /// <summary>
        /// the index of the node which is used as start point during edge creation
        /// </summary>
        private int _parentNodeOfTempEdge = -1;

        /// <summary>
        /// position of the referential (for drag purpose)
        /// </summary>
        private Vector2 _originPosition = new Vector2();

        /// <summary>
        /// the old position of the mouse during a drag (in order to calculate delta)
        /// </summary>
        private Vector2 _previousDragPosition;

        /// <summary>
        /// Direction of the edges
        /// </summary>
        protected EdgeDirection edgeDirection = EdgeDirection.HORIZONTAL;

        private bool _autoResizeWindowHeight = true;

        /// <summary>
        /// Define the default size of a node window here
        /// </summary>
        /// <returns></returns>

        protected Vector2 nodeSize = new Vector2(150, 50);
        
        /// <summary>
        /// Define the default size of an edge window here
        /// </summary>
        /// <returns></returns>
        protected Vector2 edgeSize = new Vector2(50, 50);

        /// <summary>
        /// the current edge selected
        /// </summary>
        private int _currentEdge = -1;

        /// <summary>
        /// Should we display the options to change the node size
        /// </summary>
        private bool displayNodeSize;

        /// <summary>
        /// The tangeant strength value
        /// </summary>
        protected int tangeantStrength = 200;

        /// <summary>
        /// Init the editor: override and define a method to set currentScriptableGraph here
        /// it will be called again when currentScriptableGraph is detected as null
        /// </summary>
        protected abstract void Init();

        #region UnityEditor
        /// <summary>
        /// Called each frame
        /// it will repaint the editor during edge creation to avoid lagging effect
        /// </summary>
        void Update()
        {
            if (_parentNodeOfTempEdge != -1)
                Repaint();
        }

        /// <summary>
        /// GUI call
        /// check data
        /// draw everything in that order:
        /// - the editor menu
        /// - the edges
        /// - the nodes
        /// then listen to events
        /// and finally clean garbage data (edge with no existing target or parents)
        /// </summary>
        void OnGUI()
        {
            if (_currentSerializedObject == null)
            {
                Init();
                return;
            }

            BeginWindows();
            _menuWindowRect = GUI.Window(0, _menuWindowRect, DrawMenuWindow, "Menu");
            
            if (target != null && targetProperty != null)
            {


                _nodeCount = targetProperty.FindPropertyRelative("nodes").arraySize;
                _edgeCount = targetProperty.FindPropertyRelative("edges").arraySize;

                List<string> edgeIdsToRemove = drawAllEdgesAndGetEdgeToClear();

                if (_parentNodeOfTempEdge != -1)
                {
                    Rect window = new Rect(target.nodes[_parentNodeOfTempEdge].windowPosition, nodeSize);
                    DrawTempEdge(window);
                }

                bool isEntryBeenFound = drawAllNodesAndCheckIfEntryOk();

                listenEvent();

                cleanData(edgeIdsToRemove, isEntryBeenFound);
            }
            EndWindows();
        }
        #endregion

        #region draw nodes & edges
        /// <summary>
        /// Draw a node window
        /// </summary>
        /// <param name="id">id of the window</param>
        void drawNodeWindow(int id)
        {
            int index = target.nodes.FindIndex(n => n.GetHashCode() == id);
            SerializedProperty serializedProperty = targetProperty.FindPropertyRelative("nodeValues").GetArrayElementAtIndex(index);
            EditorGUI.BeginChangeCheck();
            DrawNodeValue(serializedProperty);
            if (EditorGUI.EndChangeCheck())
                _currentSerializedObject.ApplyModifiedProperties();
            listenNodeEvent(index);
            GUI.DragWindow();
        }


        /// <summary>
        /// Draw the edge window
        /// </summary>
        /// <param name="id">id of the window</param>
        private void drawEdgeWindow(int id)
        {
            int index = target.edges.FindIndex(e => e.GetHashCode() == id);
            EditorGUI.BeginChangeCheck();
            SerializedProperty edgeValues = targetProperty.FindPropertyRelative("edgeValues");
            if (index < edgeValues.arraySize)
            {
                SerializedProperty serializedProp = edgeValues.GetArrayElementAtIndex(index);
                DrawEdgeValue(serializedProp);
            }
            if (EditorGUI.EndChangeCheck())
                _currentSerializedObject.ApplyModifiedProperties();
            if (Event.current.isMouse && Event.current.button == 0)
                _currentEdge = index;
            GUI.DragWindow();
        }

        /// <summary>
        /// draw an edge between two rectangle, overide this if you want to change the display
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        private void drawEdge(Rect start, Rect end, Vector2 offset)
        {
            Vector2 vStartPercentage, vEndPercentage;
            switch (edgeDirection)
            {
                case EdgeDirection.INVERTED_HORIZONTAL:
                    vStartPercentage = new Vector2(0.0f, 0.5f);
                    vEndPercentage = new Vector2(1.0f, 0.5f);
                    break;
                case EdgeDirection.VERTICAL:
                    vStartPercentage = new Vector2(.5f, 1f);
                    vEndPercentage = new Vector2(0.5f, 0f);
                    break;
                case EdgeDirection.INVERTED_VERTICAL:
                    vStartPercentage = new Vector2(0.5f, 0f);
                    vEndPercentage = new Vector2(0.5f, 1f);
                    break;
                default:
                    vStartPercentage = new Vector2(1.0f, 0.5f);
                    vEndPercentage = new Vector2(0.0f, 0.5f);
                    break;
            }

            DrawNodeCurve(start, end, offset, vStartPercentage, vEndPercentage);
        }
        /// <summary>
        /// Draw all nodes and check whether the defined entry node exists
        /// </summary>
        /// <returns>true if the entry node has been found, false otherwise</returns>
        private bool drawAllNodesAndCheckIfEntryOk()
        {
            bool isEntryBeenFound = false;
            for (int i = 0; i < _nodeCount; i++)
            {
                SerializedProperty windowProperty = targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex(i).FindPropertyRelative("windowPosition");
                Rect nodeWindowRect = new Rect(target.nodes[i].windowPosition, nodeSize);

                if (target.entryID == target.nodes[i].id)
                {
                    DrawEntryFeedback(nodeWindowRect);
                    isEntryBeenFound = true;
                }
                Rect absolutePosition = new Rect(nodeWindowRect.position - _originPosition, nodeWindowRect.size);
                int id = target.nodes[i].GetHashCode();
                absolutePosition = GUILayout.Window(id, absolutePosition, drawNodeWindow, target.nodes[i].id);

                target.nodes[i].windowPosition = absolutePosition.position + _originPosition;
                windowProperty.vector2Value = target.nodes[i].windowPosition;

            }

            return isEntryBeenFound;
        }

        /// <summary>
        /// Draw all egdes and return a list of edge id that has been detected has garbage
        /// </summary>
        /// <returns>a list of edge id to remove</returns>
        private List<string> drawAllEdgesAndGetEdgeToClear()
        {
            List<string> result = new List<string>();
            for (int i = 0; i < _edgeCount; i++)
            {
                int index1 = target.nodes.FindIndex(n => n.edgeIds.Contains(target.edges[i].id));
                if (index1 == -1)
                {
                    result.Add(target.edges[i].id);
                    continue;
                }
                Rect window1 = new Rect(target.nodes[index1].windowPosition - _originPosition, nodeSize);
                int index2 = target.nodes.FindIndex(n => n.id == target.edges[i].targetId);
                if (index2 == -1)
                {
                    result.Add(target.edges[i].id);
                    continue;
                }
                Rect window2 = new Rect(target.nodes[index2].windowPosition - _originPosition, nodeSize);
                Handles.color = _currentEdge == i ? Color.red : Color.black;
                drawEdge(window1, window2, target.edges[i].posOffset); // Here the curve is drawn under the windows
                if (_shouldDisplayEdgeWindows)
                {
                    Vector2 middle = (window1.center + window2.center) * .5f;
                    SerializedProperty offsetProperty = targetProperty.FindPropertyRelative("edges").GetArrayElementAtIndex(i).FindPropertyRelative("posOffset");
                    Vector2 position = (window1.center + window2.center - edgeSize) * .5f;
                    position.x = Mathf.Round(position.x);
                    position.y = Mathf.Round(position.y);
                    Rect edgeRect = new Rect(position + target.edges[i].posOffset, edgeSize);
                    edgeRect = GUILayout.Window(target.edges[i].GetHashCode(), edgeRect, drawEdgeWindow, target.edges[i].id);
                    target.edges[i].posOffset = edgeRect.position - position;
                    offsetProperty.vector2Value = target.edges[i].posOffset;
                }
                Handles.color = Color.black;
            }
            return result;
        }

        /// <summary>
        /// Clean the data of the current graph
        /// </summary>
        /// <param name="pEdgeIdsToRemove">ids of the edges to remove</param>
        /// <param name="pIsEntryBeenFound">if true the entryId will be clear</param>
        private void cleanData(List<string> pEdgeIdsToRemove, bool pIsEntryBeenFound)
        {
            if (pIsEntryBeenFound && pEdgeIdsToRemove.Count == 0)
            {
                return;
            }

            if (!pIsEntryBeenFound)
            {

                targetProperty.FindPropertyRelative("entryID").stringValue =
                    _nodeCount == 0 ?
                    "" :
                    targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex(0).FindPropertyRelative("id").stringValue;
            }
            while (pEdgeIdsToRemove.Count != 0)
            {
                int nodeIndexToClean = target.nodes.FindIndex(n => n.edgeIds.Contains(pEdgeIdsToRemove[0]));
                if (nodeIndexToClean != -1)
                {
                    SerializedProperty nodeToCleanSP = targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex(nodeIndexToClean);
                    SerializedProperty egeIds = nodeToCleanSP.FindPropertyRelative("edgeIds");
                    for (int i = egeIds.arraySize - 1; i >= 0; i--)
                    {
                        if (egeIds.GetArrayElementAtIndex(i).stringValue == pEdgeIdsToRemove[0])
                        {
                            egeIds.DeleteArrayElementAtIndex(i);
                            break;
                        }
                    }
                }
                SerializedProperty edgesSP = targetProperty.FindPropertyRelative("edges");
                SerializedProperty edgeValuesSP = targetProperty.FindPropertyRelative("edgeValues");
                for (int i = edgesSP.arraySize - 1; i >= 0; i--)
                {
                    if (edgesSP.GetArrayElementAtIndex(i).FindPropertyRelative("id").stringValue == pEdgeIdsToRemove[0])
                    {
                        edgesSP.DeleteArrayElementAtIndex(i);
                        edgeValuesSP.DeleteArrayElementAtIndex(i);
                        break;
                    }
                }
                pEdgeIdsToRemove.RemoveAt(0);
            }
        }
        #endregion

        #region events


        /// <summary>
        /// Listen to the editor view event
        /// </summary>
        private void listenEvent()
        {
            if (_parentNodeOfTempEdge != -1)
            {
                if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.Escape
                    || Event.current.button == 1)
                {
                    _parentNodeOfTempEdge = -1;
                    Event.current.Use();
                }
            }

            if (Event.current.isMouse)
            {
                _currentEdge = -1;
                if (Event.current.button == 2)
                {
                    if (Event.current.type == EventType.MouseDown)
                    {
                        _previousDragPosition = Event.current.mousePosition;
                        Event.current.Use();
                    }
                    else if (Event.current.type == EventType.MouseDrag)
                    {
                        _originPosition += _previousDragPosition - Event.current.mousePosition;
                        _previousDragPosition = Event.current.mousePosition;
                        Event.current.Use();
                    }
                }
            }


            if (Event.current.type == EventType.ContextClick)
            {
                GenericMenu g = new GenericMenu();
                g.AddItem(new GUIContent("Create new node"), false, onCreateNewNode, Event.current.mousePosition);
                g.AddSeparator("");
                g.AddItem(new GUIContent("Remove all edges..."), false, onRemoveAllEdges);
                g.AddItem(new GUIContent("Reset..."), false, onReset);
                g.ShowAsContext();
                Event.current.Use();
            }
        }


        /// <summary>
        /// Listen event on a node
        /// </summary>
        /// <param name="idx">index of the node where the Event is happening</param>
        private void listenNodeEvent(int idx)
        {
            //Debug.Log(string.Format("node's index: {0}, event: {1}", idx, Event.current));
            if (Event.current.type == EventType.MouseUp && Event.current.button == 0 && _parentNodeOfTempEdge != -1)
            {
                createEdgeFromTo(_parentNodeOfTempEdge, idx);
                _currentEdge = -1;
            }
            else if (Event.current.type == EventType.MouseUp && Event.current.button == 1)
            {
                GenericMenu g = new GenericMenu();
                g.AddItem(new GUIContent("Set as entry"), false, onSetAsEntry, (idx));
                g.AddItem(new GUIContent("Start edge"), false, onStartEdge, idx);
                g.AddSeparator("");
                g.AddItem(new GUIContent("Remove node..."), false, onRemoveNode, idx);
                g.AddItem(new GUIContent("Remove node's edges..."), false, onRemoveNodesEdges, idx);
                g.ShowAsContext();
                _currentEdge = -1;
                Event.current.Use();
            }
            else if (Event.current.type == EventType.MouseUp) // save windows drag
            {
                _currentEdge = -1;
                _currentSerializedObject.ApplyModifiedProperties();
            }
        }

        #region event callbacks
        /// <summary>
        /// "Remove node..." callback
        /// </summary>
        /// <param name="pId">node's id</param>
        private void onRemoveNode(object pId)
        {
            int idx = (int)pId;

            targetProperty.FindPropertyRelative("nodeValues").DeleteArrayElementAtIndex(idx);
            targetProperty.FindPropertyRelative("nodes").DeleteArrayElementAtIndex(idx);
            _currentSerializedObject.ApplyModifiedProperties();

            Repaint();
        }


        /// <summary>
        /// "Remove node's edges..." callback
        /// </summary>
        /// <param name="pId">node's id</param>
        private void onRemoveNodesEdges(object pId)
        {
            int idx = (int)pId;
            SerializedProperty node = targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex(idx);
            node.FindPropertyRelative("edgeIds").ClearArray();
            _currentSerializedObject.ApplyModifiedProperties();
            Repaint();
        }

        /// <summary>
        /// "Start edge" callback
        /// </summary>
        /// <param name="pId">node's id</param>
        private void onStartEdge(object pId)
        {
            _parentNodeOfTempEdge = (int)pId;
        }

        /// <summary>
        /// "Set as entry" callback
        /// </summary>
        /// <param name="pId">node's id</param>
        private void onSetAsEntry(object pId)
        {
            targetProperty.FindPropertyRelative("entryID").stringValue = targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex((int)pId).FindPropertyRelative("id").stringValue;
            _currentSerializedObject.ApplyModifiedProperties();
        }


        /// <summary>
        /// "Remove node..." callback
        /// </summary>
        /// <param name="pId">node's id</param>
        private void createEdgeFromTo(int parentNodeOfTempEdge, int idx)
        {
            string newEdgeId = GetUniqueID();

            targetProperty.FindPropertyRelative("edgeValues").InsertArrayElementAtIndex(_edgeCount);
            targetProperty.FindPropertyRelative("edges").InsertArrayElementAtIndex(_edgeCount);

            SerializedProperty newEdge = targetProperty.FindPropertyRelative("edges").GetArrayElementAtIndex(_edgeCount);
            newEdge.FindPropertyRelative("id").stringValue = newEdgeId;
            newEdge.FindPropertyRelative("targetId").stringValue = target.nodes[idx].id;

            SerializedProperty nodesEdgeIds = targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex(parentNodeOfTempEdge).FindPropertyRelative("edgeIds");
            int edgCount = nodesEdgeIds.arraySize;
            nodesEdgeIds.InsertArrayElementAtIndex(edgCount);
            nodesEdgeIds.GetArrayElementAtIndex(edgCount).stringValue = newEdgeId;

            _currentSerializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// "Reset..." callback
        /// </summary>
        private void onReset()
        {
            if (EditorUtility.DisplayDialog("Confirm...", "Are you sure you want to remove all data on this graph?", "yes", "no"))
            {
                targetProperty.FindPropertyRelative("GraphID").stringValue = "";
                targetProperty.FindPropertyRelative("entryID").stringValue = "";

                targetProperty.FindPropertyRelative("nodes").ClearArray();
                targetProperty.FindPropertyRelative("nodeValues").ClearArray();
                targetProperty.FindPropertyRelative("edges").ClearArray();
                targetProperty.FindPropertyRelative("edgeValues").ClearArray();
                _currentSerializedObject.ApplyModifiedProperties();
            }
        }

        /// <summary>
        /// "Remove all edges..." callback
        /// </summary>
        private void onRemoveAllEdges()
        {
            if (EditorUtility.DisplayDialog("Confirm...", "Are you sure you want to remove all edges data on this graph?", "yes", "no"))
            {
                targetProperty.FindPropertyRelative("edges").ClearArray();
                targetProperty.FindPropertyRelative("edgeValues").ClearArray();
                _currentSerializedObject.ApplyModifiedProperties();
            }
        }

        /// <summary>
        /// "Create new node" callback
        /// </summary>
        private void onCreateNewNode(object pPosition)
        {
            targetProperty.FindPropertyRelative("nodes").InsertArrayElementAtIndex(_nodeCount);

            SerializedProperty newNode = targetProperty.FindPropertyRelative("nodes").GetArrayElementAtIndex(_nodeCount);
            newNode.FindPropertyRelative("id").stringValue = GetUniqueID();
            newNode.FindPropertyRelative("edgeIds").ClearArray();
            newNode.FindPropertyRelative("windowPosition").vector2Value = (Vector2)pPosition + _originPosition;

            targetProperty.FindPropertyRelative("nodeValues").InsertArrayElementAtIndex(_nodeCount);

            _currentSerializedObject.ApplyModifiedProperties();
        }

        #endregion

        #endregion

        #region virtual methods
        /// <summary>
        /// Get a unique id string, if not override it will use DateTime.Now in ms in hexadecimal as an id
        /// </summary>
        /// <returns>the unique id</returns>
        virtual protected string GetUniqueID()
        {
            return (DateTime.Now.Ticks / 1000).ToString("x");
        }


        virtual protected void DrawEdgeValue(SerializedProperty serializedProp)
        {
            EditorGUILayout.PropertyField(serializedProp, true);
        }

        /// <summary>
        /// Draw the Menu window, override this to customize the menu
        /// </summary>
        /// <param name="id">id of the window</param>
        virtual protected void DrawMenuWindow(int id)
        {
            EditorGUILayout.BeginVertical();
            displayNodeSize = EditorGUILayout.Foldout(displayNodeSize, "Set Size");
            if(displayNodeSize)
            {
                nodeSize = EditorGUILayout.Vector2Field("Node window size", nodeSize);
                edgeSize = EditorGUILayout.Vector2Field("Edge window size", edgeSize);
                tangeantStrength = EditorGUILayout.IntSlider(tangeantStrength, 0, 300);
            }
            SerializedProperty serializedGraph = _currentSerializedObject.FindProperty("serializedGraphs");
            int size = serializedGraph.arraySize;
            _shouldDisplayEdgeWindows = EditorGUILayout.Toggle("edit edges", _shouldDisplayEdgeWindows);
            edgeDirection = (EdgeDirection) EditorGUILayout.EnumPopup("Direction", edgeDirection) ;
            if (GUILayout.Button("Create"))
            {
                serializedGraph.InsertArrayElementAtIndex(size);
                SerializedProperty newGraph = serializedGraph.GetArrayElementAtIndex(size);
                newGraph.FindPropertyRelative("GraphID").stringValue = "new_graph";
                newGraph.FindPropertyRelative("entryID").stringValue = "";
                newGraph.FindPropertyRelative("nodes").ClearArray();
                newGraph.FindPropertyRelative("nodeValues").ClearArray();
                newGraph.FindPropertyRelative("edges").ClearArray();
                newGraph.FindPropertyRelative("edgeValues").ClearArray();
                _currentSerializedObject.ApplyModifiedProperties();
                size++;
            }
            var entry = target == null ? null: target.nodes.Find(n => n.id == target.entryID);
            
            GUI.enabled = entry != null;
            if(GUILayout.Button("Find Entry"))
            {
                Vector2 wishPosition= this.position.size*.5f;
                wishPosition.x = Mathf.Round(wishPosition.x);
                wishPosition.y = Mathf.Round(wishPosition.y);
                _originPosition =  entry.windowPosition - wishPosition;
            }
            GUI.enabled = true;
            
            if (size != 0)
            {
                EditorGUILayout.LabelField("Graph ID");
                EditorGUI.BeginChangeCheck();

                EditorGUILayout.PropertyField(serializedGraph.GetArrayElementAtIndex(_currentSelection).FindPropertyRelative("GraphID"), GUIContent.none, false);

                _menusScrollViewPosition = EditorGUILayout.BeginScrollView(_menusScrollViewPosition);
                string[] texts = new string[size];
                for (int i = 0; i < size; ++i)
                {
                    texts[i] = serializedGraph.GetArrayElementAtIndex(i).FindPropertyRelative("GraphID").stringValue;
                }

                _currentSelection = GUILayout.SelectionGrid(_currentSelection, texts, 1);
                    

                if (EditorGUI.EndChangeCheck())
                    _currentSerializedObject.ApplyModifiedProperties();

                target = CurrentScriptableGraph.serializedGraphs[_currentSelection];
                targetProperty = serializedGraph.GetArrayElementAtIndex(_currentSelection);
                EditorGUILayout.EndScrollView();
                if(GUILayout.Button("Export Json"))
                {
                    string path = EditorUtility.SaveFilePanelInProject(this.GetType().ToString(), CurrentScriptableGraph.GetType().ToString(), "json", "Export descriptor in JSon");
                    if (!string.IsNullOrEmpty(path))
                        File.WriteAllText(path, JsonUtility.ToJson(CurrentScriptableGraph));
                }
            }
            EditorGUILayout.EndVertical();
            GUI.DragWindow();
        }

        virtual protected void DrawNodeValue(SerializedProperty serializedProperty)
        {
            EditorGUILayout.PropertyField(serializedProperty, true);
        }
        
        virtual protected void DrawNodeCurve(Rect start, Rect end, Vector2 pOffset, Vector2 vStartPercentage, Vector2 vEndPercentage)
        {
            Vector2 dir = edgeDirection >= EdgeDirection.VERTICAL ? Vector2.up : Vector2.right;
            Vector2 perp = dir == Vector2.up ? Vector2.right : Vector2.up;
            int sens = edgeDirection == EdgeDirection.VERTICAL || edgeDirection == EdgeDirection.HORIZONTAL ? 1 : -1;

            Vector2 startPos = new Vector2(start.x + start.width * vStartPercentage.x, start.y + start.height * vStartPercentage.y);
            Vector2 endPos = new Vector2(end.x + end.width * vEndPercentage.x, end.y + end.height * vEndPercentage.y);
            Vector2 middlePos = (start.center + end.center) * .5f + pOffset;
            Vector2 startTan = startPos + tangeantStrength * sens*dir;
            Vector2 endTan = endPos - tangeantStrength * sens*dir;
            Vector2 middleTan = (endTan - startTan)/3f;

            Color shadowCol = new Color(0, 0, 0, 0.06f);
            for (int i = 0; i < 3; i++) // Draw a shadow
            {
                Handles.DrawBezier(startPos, middlePos, startTan, middlePos - middleTan * .5f, shadowCol, null, (i + 1) * 5);
                Handles.DrawBezier( middlePos, endPos, middlePos + middleTan * .5f, endTan, shadowCol, null, (i + 1) * 5);
            }
            Handles.DrawBezier(startPos, middlePos, startTan, middlePos - middleTan*.5f, Handles.color, null, 2);
            Handles.DrawBezier(middlePos, endPos, middlePos + middleTan *.5f, endTan, Handles.color, null, 2);
            Handles.DrawAAConvexPolygon(
                endPos,
                endPos - sens * (15*dir + 4*perp),
                endPos - sens * (15*dir - 4*perp),
                endPos
                );
        }

        /// <summary>
        /// draw an edge between a rectangle and the mouse position, overide this if you want to change the display
        /// </summary>
        /// <param name="start">start rectangle</param>
        virtual protected void DrawTempEdge(Rect start)
        {
            Handles.color = Color.red;
            Handles.DrawLine(start.center - _originPosition, Event.current.mousePosition);
        }

        /// <summary>
        /// Draw the entry feedback, overide this if you want to change the display
        /// </summary>
        /// <param name="start"></param>
        virtual protected void DrawEntryFeedback(Rect start)
        {
            Handles.color = Color.blue;
            Handles.DrawAAConvexPolygon(start.position - 10f * Vector2.one - _originPosition,
                start.position + new Vector2(start.width + 10f, -10f) - _originPosition,
                start.position + start.size + 10f * Vector2.one - _originPosition,
                start.position + new Vector2(-10f, start.height + 10f) - _originPosition,
                start.position - 10f * Vector2.one - _originPosition);
        }

        #endregion
    }
}
