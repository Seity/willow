﻿/**
 * Copyright (c) 2016 MultiFred
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

using UnityEngine;
using UnityEditor;
using System.IO;

namespace Willow.Unity
{
    /// <summary>
    /// Editor window generate for you all the .cs file you need
    /// </summary>
    public class GraphWizard : EditorWindow
    {
        /// <summary>
        /// The path to the editor template
        /// </summary>
        static string willowEditorTemplatePath = "";

        /// <summary>
        /// the models template
        /// </summary>
        static string willowDescriptorTemplatePath = "";
        
        /// <summary>
        /// a struct that check valid info
        /// </summary>
        private struct TypeStringHelper
        {
            internal string mFeatureName;
            internal string mNodeTypeString;
            internal string mEdgeTypeString;
            internal bool isOk()
            {
                return !(string.IsNullOrEmpty(mFeatureName) || string.IsNullOrEmpty(mNodeTypeString) || string.IsNullOrEmpty(mEdgeTypeString));
            }
        }

        /// <summary>
        /// the folder path of your project
        /// </summary>
        string mFolderPath = Application.dataPath + "/";

        /// <summary>
        /// The helper
        /// </summary>
        TypeStringHelper helper;


        /// <summary>
        /// The menu item in Unity to create the graph of your dream
        /// </summary>
        [MenuItem("Willow/Create Custom Graph...", priority = 1000)]
        static void createCustomGraph()
        {
            GraphWizard window = GetWindow<GraphWizard>();
            window.Show();
        }

        /// <summary>
        /// Validation method for the menu item Willow>Create Custom Graph...
        /// </summary>
        /// <returns></returns>
        [MenuItem("Willow/Create Custom Graph...", priority = 1000, validate =true)]
        static bool validateCustomGraph()
        {
            string[] paths = Directory.GetFiles(Application.dataPath, "WillowTemplate*.txt", SearchOption.AllDirectories);
            willowDescriptorTemplatePath = "";
            willowEditorTemplatePath = "";
            foreach(string p in paths)
            {
                if (p.EndsWith("WillowTemplateDescriptor.txt"))
                    willowDescriptorTemplatePath = p.Replace("\\", "/");
                else if (p.EndsWith("WillowTemplateEditor.txt"))
                    willowEditorTemplatePath = p.Replace("\\", "/");

            }

            return !string.IsNullOrEmpty(willowDescriptorTemplatePath) && !string.IsNullOrEmpty(willowEditorTemplatePath);
        }

        /// <summary>
        /// Unity's OnGUI method refreshing the EditorWindow
        /// </summary>
        void OnGUI()
        {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.HelpBox("Enter the type using the format <Package>.<Package>.[...].<ClassName>", MessageType.Info);
                helper.mFeatureName = EditorGUILayout.TextField("Feature name", helper.mFeatureName);
                helper.mNodeTypeString = EditorGUILayout.TextField("Node type", helper.mNodeTypeString);
                helper.mEdgeTypeString = EditorGUILayout.TextField("Edge type", helper.mEdgeTypeString);

                EditorGUILayout.BeginHorizontal();
                {
                    GUI.enabled = helper.isOk();
                    if (GUILayout.Button("Generate"))
                    {
                        generateCode(helper);
                        AssetDatabase.Refresh();
                    }
                    GUI.enabled = true;
                    if (GUILayout.Button("Select destination folder"))
                    {
                        mFolderPath = EditorUtility.OpenFolderPanel("Destination folder", mFolderPath, "");
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// Generate code from template
        /// </summary>
        /// <param name="pHelper"></param>
        private void generateCode(TypeStringHelper pHelper)
        {
            generateFileFromTemplate(pHelper, willowDescriptorTemplatePath, mFolderPath, helper.mFeatureName+"Descritor");
            generateFileFromTemplate(pHelper, willowEditorTemplatePath, mFolderPath+"/Editor", helper.mFeatureName+ "Editor");
        }

        /// <summary>
        /// Generate a .cs file from the template
        /// </summary>
        /// <param name="pHelper"></param>
        /// <param name="pTemplatePath"></param>
        /// <param name="pDestination"></param>
        /// <param name="pFileName"></param>
        private void generateFileFromTemplate(TypeStringHelper pHelper, string pTemplatePath, string pDestination, string pFileName)
        {
            string output = "";
            using (StreamReader sr = new StreamReader(pTemplatePath))
            {
                while (sr.Peek() >= 0)
                {
                    string line = sr.ReadLine();
                    
                    line = line.Replace("$FEATURE_NAME$", pHelper.mFeatureName)
                        .Replace("$NODE_VALUE_TYPE$", pHelper.mNodeTypeString)
                        .Replace("$EDGE_VALUE_TYPE$", pHelper.mEdgeTypeString);
                    output += line + "\n";
                }
                sr.Close();
            }
            if (!Directory.Exists(pDestination))
                Directory.CreateDirectory(pDestination);
            
            File.WriteAllText(pDestination + "/" + pFileName + ".cs", output);
        }
    }
}