﻿/**
 * Copyright (c) 2016 MultiFred
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */


using System;
using System.Collections.Generic;
using System.Linq;

namespace Willow.Model
{
    #region graph
    /// <summary>
    /// The class describing a graph
    /// </summary>
    /// <typeparam name="TValue">the type of the values: it can be anything</typeparam>
    /// <typeparam name="TNode">the type of the nodes: it have to inherit from <![CDATA[AbstractNode<TValue, TNode, TEdge>]]></typeparam>
    /// <typeparam name="TEdge">the type of the edges: it must implement <![CDATA[IEdge<TNode>]]> and implement a default contructore (new())</typeparam>
    public abstract class Graph<TNode, TNodeValue, TEdge, TEdgeValue>
        where TEdge : Edge<TNode, TNodeValue, TEdge, TEdgeValue>
        where TNode : Node<TNode, TNodeValue, TEdge, TEdgeValue>
    {
        /// <summary>
        /// Entry point of the graph
        /// </summary>
        public TNode Entry { get; set; }

        /// Find all nodes that met the conditions specified in the predicate
        /// </summary>
        /// <param name="pPredicate"></param>
        /// <returns></returns>
        public TNode FindNode(Predicate<TNode> pPredicate)
        {
            HashSet<TNode> openSet = new HashSet<TNode>();
            openSet.Add(Entry);

            HashSet<TNode> closeSet = new HashSet<TNode>();

            while (openSet.Count != 0)
            {
                TNode current = openSet.First();
                if (pPredicate.Invoke(current))
                    return current;

                openSet.Remove(current);
                closeSet.Add(current);
                if (current.Edges != null)
                {
                    foreach (TEdge edge in current.Edges)
                    {
                        if (edge.TargetNode != null)
                        {
                            if (closeSet.Contains((TNode)edge.TargetNode))
                                continue;

                            openSet.Add((TNode)edge.TargetNode);
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Find all nodes that met the conditions specified in the predicate
        /// </summary>
        /// <typeparam name="TNode">The type of the Node you are working on</typeparam>
        /// <typeparam name="TCollection">A Type that implement IEnumerable<TNode>, ICollection<TNode>, new()</typeparam>
        /// <param name="pPredicate">the predicate to solve</param>
        /// <returns>a collection of node that validate the predicate, empty if none are found</returns>
        public TNode[] FindAllNodes(Predicate<TNode> pPredicate)
        {
            HashSet<TNode> result = new HashSet<TNode>();
            HashSet<TNode> openSet = new HashSet<TNode>();
            openSet.Add(Entry);

            HashSet<TNode> closeSet = new HashSet<TNode>();

            while (openSet.Count != 0)
            {
                TNode current = openSet.First();
                if (pPredicate.Invoke(current))
                    result.Add(current);

                openSet.Remove(current);
                closeSet.Add(current);
                if (current.Edges != null)
                {
                    foreach (TEdge edge in current.Edges)
                    {
                        if (edge.TargetNode != null)
                        {
                            if (closeSet.Contains((TNode)edge.TargetNode))
                                continue;

                            openSet.Add((TNode)edge.TargetNode);
                        }
                    }
                }
            }
            return result.ToArray();
        }
    }
    #endregion

    #region edges

    /// <summary>
    /// the edge class
    /// </summary>
    /// <typeparam name="TNode">the type of the Node</typeparam>
    public abstract class Edge<TNode, TNodeValue, TEdge, TEdgeValue>
        where TEdge : Edge<TNode, TNodeValue, TEdge, TEdgeValue>
        where TNode : Node<TNode, TNodeValue, TEdge, TEdgeValue>
    {

        /// <summary>
        /// unique id of the edge
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the target node
        /// </summary>
        public TNode TargetNode
        {
            get; set;
        }

        public TEdgeValue Value { get; set; }
        
    }
    

    #endregion

    #region Nodes
    /// <summary>
    /// The class describing a node with edge type and the value type known
    /// </summary>
    /// <typeparam name="TNodeValue">the type of the values: it can be anything</typeparam>
    /// <typeparam name="TEdge">the type of the edges</typeparam>
    public abstract class Node<TNode, TNodeValue, TEdge, TEdgeValue>
        where TEdge: Edge<TNode, TNodeValue, TEdge, TEdgeValue>
        where TNode: Node<TNode, TNodeValue, TEdge, TEdgeValue>
    {
        /// <summary>
        /// all typed Edges
        /// </summary>
        public List<TEdge> Edges { get; set; }

        /// <summary>
        /// the typed node value
        /// </summary>
        public TNodeValue Value { get; set; }

        /// <summary>
        /// Find a node that met the conditions specified in the predicate
        /// </summary>
        /// <param name="pPredicate">the predicate to invoke</param>
        /// <typeparam name="TNode">the type of the node you are looking for: expected the current Type you are manipulating</typeparam>
        /// <returns>the first Node found that validate the predicate, null otherwise</returns>
        public TNode FindNodeInChildren(Predicate<TNode> pPredicate)
        {
            HashSet<TNode> openSet = new HashSet<TNode>();
            openSet.Add((TNode)this);

            HashSet<TNode> closeSet = new HashSet<TNode>();

            while (openSet.Count != 0)
            {
                TNode current = openSet.First();
                if (pPredicate.Invoke(current))
                    return current;

                openSet.Remove(current);
                closeSet.Add(current);
                if (current.Edges != null)
                {
                    foreach (TEdge edge in current.Edges)
                    {
                        if (edge.TargetNode != null)
                        {
                            if (closeSet.Contains((TNode)edge.TargetNode))
                                continue;

                            openSet.Add((TNode)edge.TargetNode);
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// unique id of the node
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// get the edges count
        /// </summary>
        /// <returns>the number of Edges</returns>
        public int GetChildrenCount() { return Edges.Count(); }

    }
    
    #endregion
}
