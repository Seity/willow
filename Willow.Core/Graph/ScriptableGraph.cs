﻿/**
 * Copyright (c) 2016 MultiFred
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using UnityEngine;
using Willow.Model;

namespace Willow.Unity
{
    public abstract class ScriptableGraphs<TSerializer, TGraph, TNode, TNodeValue, TEdge, TEdgeValue> : ScriptableObject
        where TSerializer : GraphSerializer<TGraph, TNode, TNodeValue, TEdge, TEdgeValue>
        where TNode : Node<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TEdge : Edge<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TGraph : Graph<TNode, TNodeValue, TEdge, TEdgeValue>, new()
    {
        public List<TSerializer> serializedGraphs;
    }

    public abstract class GraphSerializer<TGraph, TNode, TNodeValue, TEdge, TEdgeValue>
        where TNode : Node<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TEdge : Edge<TNode, TNodeValue, TEdge, TEdgeValue>, new()
        where TGraph : Graph<TNode, TNodeValue, TEdge, TEdgeValue>, new()
    {
        public string GraphID;

        private TGraph currentGraph;

        public TGraph GetCurrentGraph()
        {
            Deserialize();
            return currentGraph;
        }

        public string entryID;
        public List<SerializableNode> nodes;
        public List<TNodeValue> nodeValues;

        public List<SerializableEdge> edges;
        public List<TEdgeValue> edgeValues;


        public void Deserialize()
        {
            Dictionary<string, TEdge> edgesTable = new Dictionary<string, TEdge>();

            Dictionary<string, TNode> nodesTable = new Dictionary<string, TNode>();
            int validNodeCount = Math.Min(nodes.Count, nodeValues.Count);
            int validEdgeCount = Math.Min(edgeValues.Count, edges.Count);
            for (int i = 0; i < validNodeCount; i++)
            {
                KeyValuePair<string, TNodeValue> currentSerializedNode = new KeyValuePair<string, TNodeValue>(nodes[i].id, nodeValues[i]);
                TNode node = new TNode();
                node.Value = currentSerializedNode.Value;
                node.Id = currentSerializedNode.Key;
                node.Edges = new List<TEdge>();
                nodesTable.Add(currentSerializedNode.Key, node);
            }

            currentGraph = new TGraph();
            currentGraph.Entry = nodesTable[entryID];

            for (int i = 0; i < validEdgeCount; i++)
            {
                KeyValuePair<string, TEdgeValue> currentSerializedEdge = new KeyValuePair<string, TEdgeValue>(edges[i].id, edgeValues[i]);

                TEdge edg = new TEdge();
                edg.Value = currentSerializedEdge.Value;
                edg.Id = currentSerializedEdge.Key;
                edgesTable.Add(currentSerializedEdge.Key, edg);
            }

            for (int i = 0; i < validNodeCount; i++)
            {
                SerializableNode currentSerializedNode = nodes[i];
                TNode currentNode = nodesTable[currentSerializedNode.id];
                if (currentSerializedNode.edgeIds != null)
                    foreach (string edgid in currentSerializedNode.edgeIds)
                    {
                        if(edgesTable.ContainsKey(edgid))
                            currentNode.Edges.Add(edgesTable[edgid]);
                    }
            }
            for (int i = 0; i < validEdgeCount; i++)
            {
                SerializableEdge currentSerializedEdge = edges[i];
                if (edgesTable.ContainsKey(currentSerializedEdge.id))
                {
                    TEdge currentEdge = edgesTable[currentSerializedEdge.id];
                    currentEdge.TargetNode = nodesTable[currentSerializedEdge.targetId];
                }
            }
        }
    }

    [Serializable]
    public class SerializableNode
    {
        public string id;
        public List<string> edgeIds;
        public Vector2 windowPosition;
    }

    [Serializable]
    public class SerializableEdge
    {
        public string id;
        public string targetId;
        public Vector2 posOffset;
    }

    [Serializable]
    public class CoupleIdNodeValue
    {
        public string id;
        public object val;
    }

    [Serializable]
    public class CoupleIdEdgeValue
    {
        public string id;
        public object val;
    }
}